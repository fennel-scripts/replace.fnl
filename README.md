# readme for replace.fnl
	this for that

usage:
	replace <pattern> <replacement>

for more info, have a look at the documentation of lua string.gsub function
the only difference is that the pattern "<br>" will be replaced with a newline as "\n" didn`t work

backend:
	the script runs string.gsub(stdin <pattern> <replacement>) and outputs the result
# installation

this is currently dependant on my botsbuildbots script for automating installation

Link: https://gitlab.com/fennel-scripts/botsbuildbots

Usage: botsbuildbots default

usage(if you dont want to install botsbuildbots to $PATH): ./botsbuildbots.fnl default



## experimental

one alternative is to run `./bots`

this method is however highly experimental and depends on fennel

 but you should already have that installed
