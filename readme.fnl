#!/bin/env fennel




(print (table.concat
		["# readme for replace.fnl"
		 "\tthis for that\n"
		 "usage:"
		 "\treplace <pattern> <replacement>\n"
		 "for more info, have a look at the documentation of lua string.gsub function"
		 "the only difference is that the pattern \"<br>\" will be replaced with a newline as \"\\n\" didn`t work"
		 "\nbackend:"
		 "\tthe script runs string.gsub(stdin <pattern> <replacement>) and outputs the result"
		 ] "\n"))


(print (table.concat
		["# installation"
		 "this is currently dependant on my botsbuildbots script for automating installation"
		 "Link: https://gitlab.com/fennel-scripts/botsbuildbots"
		 "Usage: botsbuildbots default"
		 "usage(if you dont want to install botsbuildbots to $PATH): ./botsbuildbots.fnl default"
		 ""
		 "## experimental"
		 "one alternative is to run `./bots`"
		 "this method is however highly experimental and depends on fennel"
		 " but you should already have that installed"
		 ] "\n\n"))
