#!/bin/env fennel


(local args
	   {
		:pattern (. arg 1)
		:with (. arg 2)
		}
	   )


(fn replace [args]

  (local lines [])
  (each [v _ (io.lines)]
	(table.insert lines v)
	)

  (print
   (pick-values 1 (string.gsub
				   (pick-values 1 (string.gsub (table.concat lines "\n") args.pattern args.with))
				   "<br>" "\n")
				)
   )
  )
(fn plzhelp []
  "print some help"
  (print (table.concat
		  ["\t\t\tthis for that\n"
		   "usage:"
		   "\treplace <pattern> <replacement>\n"
		   "for more info, have a look at the documentation of lua string.gsub function"
		   "the only difference is that the pattern \"<br>\" will be replaced with a newline as \"\\n\" didn`t work"
		   "\nbackend:"
		   "\tthe script runs string.gsub(stdin <pattern> <replacement>) and outputs the result"
		   ] "\n"))

  )


(match (. arg 1)
  :-h (plzhelp)
  :--help (plzhelp)
  _ (replace args)
  )


nil
